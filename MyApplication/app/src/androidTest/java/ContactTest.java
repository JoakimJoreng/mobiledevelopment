import android.provider.ContactsContract;
import android.test.AndroidTestCase;


import com.example.joakim.myapplication.Contact;
import com.example.joakim.myapplication.Contacts;
import com.example.joakim.myapplication.DatabaseHandler;

/**
 * Created by Joakim on 16.09.2014.
 *
 */
public class ContactTest extends AndroidTestCase{

    public void test() {

        String name = "John Doe";
        String phone = "41697805";
        String addInfo = "Cool dude";
        double latitude = 10.52123;
        double longitude = 8.92822;
        String location = "Hamar";

        Contact contact1 = new Contact(name, phone, addInfo, latitude, longitude, location);
        Contact contact2 = new Contact(name, phone, addInfo, latitude, longitude, location);

        assertEquals(0, contact1.getId());
        assertEquals(name, contact1.getName());
        assertEquals(phone, contact1.getPhone());
        assertEquals(addInfo, contact1.getAdditionalInfo());
        assertEquals(latitude, contact1.getLatitude());
        assertEquals(longitude, contact1.getLongitude());
        assertEquals(location, contact1.getLocation());

        Contacts contacts = new Contacts(new DatabaseHandler(getContext()));
        contacts.addContact(contact1);
        contacts.addContact(contact2);

        assertEquals(1, contacts.getContactList().get(0).getId());
    }
}
