package com.example.joakim.myapplication;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;


/**
 * Created by Joakim on 13.09.2014.
 * Keeps track of the users whereabouts
 */
public class LocationHandler {

    private LocationManager locationManager;
    private LocationListener locationListener;
    private LocationUpdateListener locationUpdateListner;
    private Geocoder geocoder;

    private String lastKnownLocation;
    private double lastKnownLatitude;
    private double lastKnownLongitude;

    public LocationHandler(final Context context, final LocationUpdateListener locationUpdateListener) {

        this.locationUpdateListner = locationUpdateListener;
        this.locationManager =  (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        this.geocoder = new Geocoder(context);
        this.locationListener = new LocationListener() {

            @Override
            public void onLocationChanged(Location location) {

                lastKnownLatitude = location.getLatitude();
                lastKnownLongitude = location.getLongitude();
                lastKnownLocation = getLocationByCoordinates(lastKnownLatitude, lastKnownLongitude);
                locationUpdateListener.onLocationUpdate(location);
                Log.w("LocationHandler", "Got location " + location.getLatitude() + "," + location.getLongitude());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

                Toast.makeText(context.getApplicationContext(), provider + " enabled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onProviderDisabled(String provider) {

                Toast.makeText(context.getApplicationContext(), provider + " disabled", Toast.LENGTH_LONG).show();
            }
        };

        if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER)) {

            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            Log.w("PROVIDER","NETWORK IS SET");
        }

        if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER)) {

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            Log.w("PROVIDER","GPS IS SET");
        }
    }


    public String getLastKnownLocation() {

        return lastKnownLocation;
    }

    public double getLastKnownLatitude() {

        return lastKnownLatitude;
    }

    public double getLastKnownLongitude() {

        return lastKnownLongitude;
    }

    /**
     *
     * @param latitude
     * @param longitude
     * Retrieves name of location based on latitude and longitude
     *
     * @return
     */
    public String getLocationByCoordinates(double latitude, double longitude) {

        String locationName = null;
        try {

            List<Address> geolocation = geocoder.getFromLocation(latitude, longitude, 1);

            if(geolocation.size() > 0) {
                if (geolocation.get(0).getSubAdminArea() != null) {

                    locationName = geolocation.get(0).getSubAdminArea();
                    if (geolocation.get(0).getSubLocality() != null) {

                        locationName += " ," + geolocation.get(0).getSubLocality();
                    }
                }
            }
        }
        catch (IOException e) {

            Log.w("LocationHandler", "Could not retrieve geolocation " + e.getMessage());
        }

        return locationName;
    }
}
