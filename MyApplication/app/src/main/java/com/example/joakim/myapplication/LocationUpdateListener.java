package com.example.joakim.myapplication;

import android.location.Location;

/**
 * Created by Joakim on 13.09.2014.
 */
public interface LocationUpdateListener {

    void onLocationUpdate(Location location);
}
