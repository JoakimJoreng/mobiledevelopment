package com.example.joakim.myapplication;

/**
 * Created by Joakim on 14.09.2014.
 *
 */
public interface NewsUpdateListener {

    void onNewsUpdate(String string);
}
