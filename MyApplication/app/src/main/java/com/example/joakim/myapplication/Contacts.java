package com.example.joakim.myapplication;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joakim on 11.09.2014.
 *
 * Contact container class
 */
public class Contacts {

    private DatabaseHandler dbHelper;
    private List<Contact> contacts;

    /**
     * @param databaseHelper
     * Populates the list on creation
     */
    public Contacts(DatabaseHandler databaseHelper) {

        this.dbHelper = databaseHelper;
        contacts = new ArrayList<Contact>();

        // Stolen from https://bitbucket.org/gtl-hig/imt3662_sql_notes/
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        final Cursor c = db.query(TABLE_CONTACT,
                new String[] { CONTACT_ID, CONTACT_NAME, CONTACT_PHONE,
                               CONTACT_ADDITIONAL_INFO, CONTACT_LATITUDE,
                               CONTACT_LONGITUDE, CONTACT_LOCATION }, null, null, null, null, null);
        // make sure you start from the first item
        c.moveToFirst();
        while (!c.isAfterLast()) {
            final Contact contact = cursorToContact(c);
            contacts.add(contact);
            c.moveToNext();
        }
        // Make sure to close the cursor
        c.close();
    }

    public List<Contact> getContactList() {

        return contacts;
    }

    /**
     * @param c
     * @return a contact object
     */
    public static Contact cursorToContact(Cursor c) {
        final Contact contact = new Contact();
        contact.setName(c.getString(c.getColumnIndex(CONTACT_NAME)));
        contact.setPhone(c.getString(c.getColumnIndex(CONTACT_PHONE)));
        contact.setAdditionalInfo(c.getString(c.getColumnIndex(CONTACT_ADDITIONAL_INFO)));
        contact.setLatitude(c.getDouble(c.getColumnIndex(CONTACT_LATITUDE)));
        contact.setLongitude(c.getDouble(c.getColumnIndex(CONTACT_LONGITUDE)));
        contact.setLocation(c.getString(c.getColumnIndex(CONTACT_LOCATION)));

        return contact;
    }

    /**
     * @param contact
     * Add a contact to the database and to the contacts list
     */
    public void addContact(Contact contact) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CONTACT_NAME, contact.getName());
        values.put(CONTACT_PHONE, contact.getPhone());
        values.put(CONTACT_ADDITIONAL_INFO, contact.getAdditionalInfo());
        values.put(CONTACT_LATITUDE, contact.getLatitude());
        values.put(CONTACT_LONGITUDE, contact.getLongitude());
        values.put(CONTACT_LOCATION, contact.getLocation());

        contact.setId(db.insert(TABLE_CONTACT, null, values));
        db.close();

        contacts.add(contact);
    }

    /**
     * @param id
     * Delete a contact from the database and the contacts list
     */
    public void deleteContact(long id) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(TABLE_CONTACT, CONTACT_ID + "=?", new String[] { String.valueOf(id) });
        db.close();

        // TODO: REMOVE FROM LIST(contacts)!
        // TODO: Implement
    }

    /**
     * Updates existing contact
     * @param contact
     * @return updateCode
     */
    public int updateContact(Contact contact) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CONTACT_NAME, contact.getName());
        values.put(CONTACT_PHONE, contact.getPhone());
        values.put(CONTACT_ADDITIONAL_INFO, contact.getAdditionalInfo());
        values.put(CONTACT_LATITUDE, contact.getLatitude());
        values.put(CONTACT_LONGITUDE, contact.getLongitude());
        values.put(CONTACT_LOCATION, contact.getLocation());

        int updateCode = db.update(TABLE_CONTACT, values, CONTACT_ID + "=?", new String[] { String.valueOf(contact.getId()) });
        db.close();

        return updateCode;

        // TODO: implement!
    }

    /**
     * getContactCount
     * @return contact count(int)
     */
    public int getContactCount() {

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM  " + TABLE_CONTACT, null);
        int count = cursor.getCount();
        db.close();

        return count;
    }

    // Constants
    static final String TABLE_CONTACT = "Contact";
    static final String CONTACT_ID = "id";
    static final String CONTACT_NAME = "name";
    static final String CONTACT_PHONE = "phone";
    static final String CONTACT_ADDITIONAL_INFO = "additionalInfo";
    static final String CONTACT_LATITUDE = "latitude";
    static final String CONTACT_LONGITUDE = "longitude";
    static final String CONTACT_LOCATION = "location";

    // Create table string
    public static final String CONTACT_CREATE_TABLE = "CREATE TABLE " + TABLE_CONTACT
            + " (" + CONTACT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CONTACT_NAME + " TEXT,"
            + CONTACT_PHONE + " TEXT,"
            + CONTACT_ADDITIONAL_INFO + " TEXT,"
            + CONTACT_LATITUDE + " DOUBLE,"
            + CONTACT_LONGITUDE + " DOUBLE,"
            + CONTACT_LOCATION + " TEXT" +
            ");";
}
