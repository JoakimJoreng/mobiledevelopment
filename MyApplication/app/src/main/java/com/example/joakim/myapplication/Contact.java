package com.example.joakim.myapplication;

import android.util.Log;

/**
 * Created by Joakim on 09.09.2014.
 *
 * Contact class containing information about a contact
 */

public class Contact {

    private long id;
    private String name;
    private String phone;
    private String additionalInfo;
    private double latitude;
    private double longitude;
    private String location;

    public Contact() { }
    public Contact(String name, String phone, String additionalInfo, double latitude, double longitude, String location) {

        this.name = name;
        this.phone = phone;
        this.additionalInfo = additionalInfo;
        this.latitude = latitude;
        this.longitude = longitude;
        this.location = location;
    }

    // Getters
    public long getId() {
        return this.id;
    }
    public String getName() {
        return this.name;
    }
    public String getPhone() {
        return this.phone;
    }
    public String getAdditionalInfo() { return this.additionalInfo; }
    public double getLatitude() { return this.latitude; }
    public double getLongitude() { return this.longitude; }
    public String getLocation() { return this.location; }

    // Setters
    public void setId(long id) { this.id = id; Log.w("ID", String.valueOf(id)); }
    public void setName(String name) { this.name = name; }
    public void setPhone(String phone) { this.phone = phone; }
    public void setAdditionalInfo(String addInfo) { this.additionalInfo = addInfo; }
    public void setLatitude(double latitude) { this.latitude = latitude; }
    public void setLongitude(double longitude) { this.longitude = longitude; }
    public void setLocation(String location) { this.location = location; }
}