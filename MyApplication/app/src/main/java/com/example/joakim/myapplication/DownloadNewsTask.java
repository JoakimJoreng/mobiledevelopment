package com.example.joakim.myapplication;

import android.os.AsyncTask;
import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

/**
 * Retrieves rss from NRK nyheter
 *
 */

public class DownloadNewsTask extends AsyncTask<String, Void, String> {

    private NewsUpdateListener newsUpdateListener;

    public DownloadNewsTask(final NewsUpdateListener newsUpdateListener) {

        this.newsUpdateListener = newsUpdateListener;
    }

    @Override
    protected String doInBackground(String... urls) {

        assert urls.length == 1; // sanity check
        return downloadNRKtopStoryTitle(urls[0]);
    }

    @Override
    protected void onPostExecute(String result) {

        newsUpdateListener.onNewsUpdate(result);
    }

    /**
     *
     * @param url retrieves downloaded webresource
     * @return title text from NRK's top story
     */
    private String downloadNRKtopStoryTitle(String url) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(url);

            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            // Getting the first item in the NRK top news feed
            Node titleNode = (Node) xpath.evaluate("//rss/channel/item[1]/title", document, XPathConstants.NODE);
            Element titleElement = (Element) titleNode;

            return titleElement.getTextContent();
        } catch (Exception e) {
            Log.w("URL", " could not read   " + url + "  ---  " + e.toString());
            return null;
        }
    }
}