package com.example.joakim.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Contact Manager
 */
public class MainActivity extends Activity implements LocationUpdateListener, NewsUpdateListener {
    private Contacts contacts;
    private LocationHandler locationHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        locationHandler = new LocationHandler(getApplicationContext(), this);

        // Starting async web request from NRK
        String url = "http://www.nrk.no/toppsaker.rss";
        DownloadNewsTask info = new DownloadNewsTask(this);
        info.execute(url);

        // Create contact list
        contacts = new Contacts(new DatabaseHandler(this));
        populateListView();

        // Setting up tab GUI
        // Tab and basic app GUI layout was taken from this tutorial
        // http://www.youtube.com/watch?v=irDdBxamuZs
        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();
        TabHost.TabSpec tabSpec = tabHost.newTabSpec("Contact Creator");
        tabSpec.setContent(R.id.contactCreatorTab);
        tabSpec.setIndicator("Contact Creator");
        tabHost.addTab(tabSpec);
        tabSpec = tabHost.newTabSpec("Contact List");
        tabSpec.setContent(R.id.contactListTab);
        tabSpec.setIndicator("Contact List");
        tabHost.addTab(tabSpec);

        // Adding buttonlistner to "add contact button"
        final Button addContactButton = (Button) findViewById(R.id.addContactButton);
        addContactButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                EditText name = (EditText) findViewById(R.id.nameTxt);
                EditText phone = (EditText) findViewById(R.id.phoneTxt);
                EditText addInfo = (EditText) findViewById(R.id.additionalInfo);

                contacts.addContact(new Contact(name.getText().toString(),
                        phone.getText().toString(),
                        addInfo.getText().toString(),
                        locationHandler.getLastKnownLatitude(),
                        locationHandler.getLastKnownLongitude(),
                        locationHandler.getLastKnownLocation()));

                Toast.makeText(getApplicationContext(), name.getText().toString() +
                               " has been added to your contacts", Toast.LENGTH_SHORT).show();

                populateListView();
                name.setText("");
                phone.setText("");
                addInfo.setText("");
                name.hasFocus();
            }
        });

        // Adding textchanged listner for Name input field
        EditText name = (EditText) findViewById(R.id.nameTxt);
        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count){}
            @Override
            public void afterTextChanged(Editable s) {

                EditText name = (EditText) findViewById(R.id.nameTxt);
                EditText phone = (EditText) findViewById(R.id.phoneTxt);

                // Enable button if name and phone has a string value
                addContactButton.setEnabled((!name.getText().toString().trim().isEmpty()) &&
                                            (!phone.getText().toString().trim().isEmpty()));
                phone.hasFocus();
            }
        });

        // Adding textchanged listner for Phone input field
        EditText phone = (EditText) findViewById(R.id.phoneTxt);
        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {

                EditText name = (EditText) findViewById(R.id.nameTxt);
                EditText phone = (EditText) findViewById(R.id.phoneTxt);
                addContactButton.setEnabled((!name.getText().toString().trim().isEmpty()) &&
                                            (!phone.getText().toString().trim().isEmpty()));
                if (name.getText().toString().trim().isEmpty()) {

                    name.hasFocus();
                }
            }
        });
    }

    // Inner class
    // Serves as adapter between the contacts and the GUI contact list
    private class ContactListAdapter extends ArrayAdapter<Contact> {

        public ContactListAdapter() {

            super(MainActivity.this, R.layout.contactlist_item, contacts.getContactList());
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView == null) {

                convertView = getLayoutInflater().inflate(R.layout.contactlist_item, parent, false);
            }

            final Contact currentContact = contacts.getContactList().get(position);

            TextView name = (TextView) convertView.findViewById(R.id.contactName);
            name.setText(currentContact.getName());

            TextView phone = (TextView) convertView.findViewById(R.id.contactPhone);
            phone.setText(currentContact.getPhone());

            TextView addInfo = (TextView) convertView.findViewById(R.id.contactAddInfo);
            addInfo.setText(currentContact.getAdditionalInfo());

            TextView location = (TextView) convertView.findViewById(R.id.contactLocation);
            location.setText(currentContact.getLocation());

            Button mapButton = (Button) convertView.findViewById(R.id.contactMapButton);
            mapButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // Adds button press listener for contact card see location button
                    // This triggers a map activity which receives the lat and long for positioning
                    Intent intent = new Intent(getApplicationContext(), LocateOnMapActivity.class);
                    intent.putExtra("LATITUDE", currentContact.getLatitude());
                    intent.putExtra("LONGITUDE", currentContact.getLongitude());
                    Log.w("MAP", ""+currentContact.getLongitude());
                    startActivity(intent);
                }
            });

            convertView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    Toast.makeText(getApplicationContext(), "Long Clicked " ,
                                   Toast.LENGTH_SHORT).show();
                    return true;
                }
            });

            return convertView;
        }
    }
    /**
     * Populates the GUI listview with information from each
     * contact in the contactlist
     */
    private void populateListView() {

        ArrayAdapter<Contact> adapter = new ContactListAdapter();
        ListView contactListView = (ListView)findViewById(R.id.contactListView);
        contactListView.setAdapter(adapter);
    }

    /**
     * Updates current location in GUI
     * @param location
     */
    @Override
    public void onLocationUpdate(Location location) {

        TextView header = (TextView) findViewById(R.id.headerLabel);
        header.setText("Create a contact in " + locationHandler.getLastKnownLocation());
    }

    /**
     * @param headline
     * Sets latest news headline from NRK
     */
    @Override
    public void onNewsUpdate(String headline) {

        Toast.makeText(getApplicationContext(), "Latest news from NRK: " + headline,
                Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

